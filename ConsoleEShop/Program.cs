﻿using System;
using System.Collections.Generic;
using ConsoleEShop.Repositories;
using ConsoleEShop.Menus;
using ConsoleEShop.Services;

namespace ConsoleEShop
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Product> products = new List<Product>() {
                new Product("Apple", "Fruit", "Greer", 1000),
                new Product("Milk", "Grocery", "White", 2000),
            };
            List<User> users = new List<User>() {
                new User("Roman", "123", new List<Order>(), Roles.User),
                new User("Vova", "111", new List<Order>(), Roles.User),
                new User("Boris", "admin", new List<Order>(), Roles.Admin)
            };

            Repository data = Repository.GetInstance(products, users, null);
            GuestMenu guestShow = new GuestMenu(data, new GuestService());
            guestShow.Start();

            Console.WriteLine("Hello User");
        }

    }
}
