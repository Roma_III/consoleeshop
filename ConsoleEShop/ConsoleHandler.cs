﻿using System;
using System.Collections.Generic;
using ConsoleEShop.Repositories;

namespace ConsoleEShop
{
    public class ConsoleHandler
    {

        public (string, string) GetUserLogin()
        {
            bool isCorect = false;
            while (!isCorect)
            {
                Console.WriteLine("Enter name");
                string Name = Console.ReadLine().Trim();
                Console.WriteLine("Enter pass");
                string Pass = Console.ReadLine().Trim();

                if (Name == null || Pass == null)
                {
                    Console.WriteLine("Incorect input");
                    continue;
                }
                return (Name, Pass);

            }
            throw new ArgumentNullException();
        }
        public User UserRegistration()
        {
            (string, string) nameAndPass = GetUserLogin();
            return new User(nameAndPass.Item1, nameAndPass.Item2, new List<Order>(), Roles.User);
        }
        public string GetNameOfProduct()
        {
            bool isCorect = false;
            string answer = "";
            while (!isCorect)
            {
                Console.WriteLine("Write produc name");
                answer = Console.ReadLine().Trim();
                if (answer == null)
                {
                    Console.WriteLine("Incorect input");
                    continue;
                }

                return answer;
            }
            throw new ArgumentNullException();

        }
        public Order ChooseOrder(List<Order> orders)
        {
            if (orders.Count == 0)
            {
                Console.WriteLine("This user haven't orders");
                return null;
            }

            bool isCorect = false;
            int index = 0;
            for (int i = 0; i < orders.Count; i++)
            {
                Console.WriteLine($"{i + 1} {orders[i]}");
            }

            while (!isCorect)
            {
                Console.WriteLine("Choose order");
                bool isCorctIndex = int.TryParse(Console.ReadLine().Trim(), out index);
                if (index < 0 || index > orders.Count || !isCorctIndex)
                {
                    Console.WriteLine("Incorect input");
                    continue;
                }
                isCorect = true;
                return orders[index - 1];
            }

            throw new ArgumentNullException();
        }
        public Order CreateNewOrder(List<Product> products)
        {
            bool isCorect = false;
            int index = 0;
            int count = 0;
            while (!isCorect)
            {
                Console.WriteLine("Choose number of product which  you want to add in your order");
                bool isCorctIndex = int.TryParse(Console.ReadLine().Trim(), out index);
                if (index < 0 || index > products.Count || !isCorctIndex)
                {
                    Console.WriteLine("Incorect input");
                    continue;
                }
                isCorect = true;
            }

            isCorect = false;
            while (!isCorect)
            {
                Console.WriteLine("How much you want to buy");

                bool isCorctCount = int.TryParse(Console.ReadLine().Trim(), out count);
                if (count < 0 || count > products.Count || !isCorctCount)
                {
                    Console.WriteLine("Incorect input");
                    continue;
                }
                isCorect = true;
                return new Order(products[index - 1], count);
            }

            throw new ArgumentException();

        }
        public OrderIsAccept ChooseAcceptOrCanceled()
        {
            string answer = null;
            bool isCorect = false;
            while (!isCorect)
            {
                Console.WriteLine("You want to accept this order Y/N");
                answer = Console.ReadLine().Trim().ToLower();
                if (answer == null)
                {
                    Console.WriteLine("Incorect input");
                    continue;
                }
                isCorect = true;
            }

            if (answer == "y")
            {
                return OrderIsAccept.Accept;
            }
            else
            {
                return OrderIsAccept.Canceled;
            }
        }
        public void ShowFunction(string[] array)
        {
            Console.WriteLine("What are you want?");
            //Use like individual method
            int count = 1;
            if (array == null)
                throw new ArgumentException();
            foreach (var item in array)
            {
                Console.WriteLine($"{count} - {item}");
                count++;
            }
        }
        public User ChoseUserForOrderStatusChanging(List<User> users)
        {
            bool isCorect = false;
            for (int i = 0; i < users.Count; i++)
            {
                Console.WriteLine($"{i + 1} {users[i]}");
            }

            while (!isCorect)
            {
                Console.WriteLine("Enter number of user whos you want to change/see");
                int user;
                bool userCorect = int.TryParse(Console.ReadLine().Trim(), out user);
                if (user < 0 || user > users.Count || !userCorect)
                {
                    Console.WriteLine("Incorect input");
                    continue;
                }
                isCorect = true;
                return users[user - 1];
            }
            throw new ArgumentNullException();

        }
        public OrderStatus ChoseOrderStatusAdmin()
        {
            bool corectData = false;
            string answer = null;
            while (!corectData)
            {
                Console.WriteLine("which status you want use for this order");
                foreach (OrderStatus order in Enum.GetValues(typeof(OrderStatus)))
                {
                    Console.WriteLine(order);
                }
                answer = Console.ReadLine().Trim();
                if (answer == null)
                {
                    Console.WriteLine("Incorect input");
                    continue;
                }
                corectData = true;
            }

            switch (answer)
            {
                case "Finish":
                    return OrderStatus.Finish;
                case "GetPayment":
                    return OrderStatus.GetPayment;
                case "Received":
                    return OrderStatus.Received;
                case "Sent":
                    return OrderStatus.Sent;
                case "CanseledByAdministrator":
                    return OrderStatus.CanseledByAdministrator;
                default:
                    return OrderStatus.New;
            }
        }
        public (User, User) GetUserData(List<User> users)
        {
            bool corectData = false;
            while (!corectData)
            {
                Console.WriteLine("Chose number of user who you want change");
                int index;
                bool isCorctIndex = int.TryParse(Console.ReadLine().Trim(), out index);
                if (index < 0 || index > users.Count || !isCorctIndex)
                {
                    Console.WriteLine("Incorect input");
                    continue;
                }

                Console.WriteLine("If you don't want to change parametr - you shouldn't enter nothing ");
                Console.WriteLine("Enter new name");
                string name = Console.ReadLine().Trim();
                name = name == "" ? users[index - 1].Name : name;

                Console.WriteLine("Enter new Pass");
                string pass = Console.ReadLine().Trim();
                pass = pass == "" ? users[index - 1].Pass : pass;

                Console.WriteLine("Do you want set admin access for this user Y/N");
                string accessStr = Console.ReadLine().Trim().ToLower();
                Roles access = accessStr != "y" ? users[index - 1].Role : Roles.Admin;
                corectData = true;

                return (new User(name, pass, null, access), users[index - 1]);
            }
            throw new ArgumentNullException();
        }
        public (Product, Product) GetProductDataForChanging(List<Product> products)
        {
            bool corectData = false;
            while (!corectData)
            {
                Console.WriteLine("Chose number of product which you want change");
                int index;
                bool isCorctIndex = int.TryParse(Console.ReadLine().Trim(), out index);
                if (index < 0 || index > products.Count || !isCorctIndex)
                {
                    Console.WriteLine("Incorect input");
                    continue;
                }

                Console.WriteLine("If you don't want to change parametr - you shouldn't enter nothing ");

                Console.WriteLine("Enter new name");
                string name = Console.ReadLine().Trim();
                name = name == "" ? products[index - 1].Name : name;


                Console.WriteLine("Enter new Category");
                string category = Console.ReadLine().Trim();
                category = category == "" ? products[index - 1].Category : category;

                Console.WriteLine("Enter new description");
                string descr = Console.ReadLine().Trim();
                descr = descr == "" ? products[index - 1].Description : descr;

                Console.WriteLine("Enter new price");

                decimal price = 0;
                bool isPrice = decimal.TryParse(Console.ReadLine().Trim(), out price);
                price = price == 0 ? products[index - 1].Price : price;

                corectData = true;
                return (new Product(name, category, descr, price), products[index - 1]);
            }

            throw new ArgumentNullException();

        }
        public Product GetProducData()
        {
            bool corectData = false;
            while (!corectData)
            {
                Console.WriteLine("Enter product name");
                string name = Console.ReadLine().Trim();
                if (name == "")
                {
                    Console.WriteLine("Incoretc input try again");
                    continue;
                }

                Console.WriteLine("Enter product category");
                string category = Console.ReadLine().Trim();
                if (category == "")
                {
                    Console.WriteLine("Incoretc input try again");
                    continue;
                }

                Console.WriteLine("Enter product Decription");
                string decrip = Console.ReadLine().Trim();
                if (decrip == "")
                {
                    Console.WriteLine("Incoretc input try again");
                    continue;
                }

                Console.WriteLine("Enter product price");
                decimal price = 0;
                bool isPrice = decimal.TryParse(Console.ReadLine().Trim(), out price);

                if (price == 0 || !isPrice)
                {
                    Console.WriteLine("Incoretc input try again");
                    continue;
                }
                corectData = true;
                return new Product(name, category, decrip, price);
            }
            throw new ArgumentNullException();
        }
    }
}
