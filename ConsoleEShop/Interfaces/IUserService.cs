﻿using System.Collections.Generic;
using ConsoleEShop.Repositories;

namespace ConsoleEShop.Interfaces
{
    public interface IUserService : IGuestService
    {
        public Order GetOrder(int number, User user);
        public void ChangeOrderStatusUser(User user, Order order);
        public void CreateNewOrder(Order order, User user);
        public void Exit(Repository data);
        public List<Order> SeeOwnOrder(User user);
        public void FinishOrder(Order order, OrderIsAccept orderIsAccept);
    }
}
