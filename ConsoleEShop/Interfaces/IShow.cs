﻿using ConsoleEShop.Repositories;

namespace ConsoleEShop.Interfaces
{
    public interface IShow
    {
        public bool IsActive { get; set; }
        Repository Data { get; set; }
        public void Start();

    }
}
