﻿using System.Collections.Generic;
using ConsoleEShop.Repositories;

namespace ConsoleEShop.Interfaces
{
    public interface IGuestService
    {
        public bool IsLogin((string, string) source, Repository data);
        public void Registration(User user, Repository data);
        public void ViewProduct(List<Product> products);
        public Product SearchProduct(string name, List<Product> products);
        void Login(Repository data, IShow interfaceShow, bool isLogin);
    }
}
