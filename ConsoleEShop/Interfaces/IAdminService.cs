﻿using ConsoleEShop.Repositories;
using System.Collections.Generic;

namespace ConsoleEShop.Interfaces
{
    public interface IAdminService : IUserService
    {
        public void AddNewProduct(Product product, List<Product> products);
        public void ChangeProductInfo(Product currentProd, Product productAfterChange, List<Product> products);
        public void ChangeUserInformation(User currentUser, User userAfterChange, List<User> users);
        public void ChangeOrderStatusAdmin(User user, Order order, OrderStatus orderStatus);
        public void SeeUserInformation(List<User> users);
        public void ChangeUserRole(User user, Roles role);
    }
}
