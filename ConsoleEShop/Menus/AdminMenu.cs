﻿using System;
using System.Collections.Generic;
using ConsoleEShop.Interfaces;
using ConsoleEShop.Repositories;
using ConsoleEShop.Services;


namespace ConsoleEShop.Menus
{
    public class AdminMenu : IShow
    {

        public bool IsActive { get; set; }
        public string[] UserAction = { "View product", "Search product", "Change order status",
            "See order", "Create new order", "See user info", "Change user info", "Change product info",
            "Add new product", "Exit" };
        public Repository Data { get; set; }

        private IShow _interfaceShow;
        private readonly IAdminService _adminService;
        private ConsoleHandler _consoleHandler;
        private Dictionary<string, Action> _adminShowAction;

        public AdminMenu(Repository data, IAdminService adminService)
        {
            Data = data;
            _adminService = adminService;
            _consoleHandler = new ConsoleHandler();
            _interfaceShow = new GuestMenu(Data, new GuestService());
        }

        public void Start()
        {
            IsActive = true;

            Console.WriteLine("Hello");
            FillInDictionary();
            while (IsActive)
            {
                Console.Clear();
                _consoleHandler.ShowFunction(UserAction);
                Console.WriteLine("Choose action");

                string action = Console.ReadLine().Trim();

                if (_adminShowAction.ContainsKey(action))
                {
                    _adminShowAction[action].Invoke();
                    Console.WriteLine("Enter for continue");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Try again");
                }
            }
        }

        private void FillInDictionary()
        {
            _adminShowAction = new Dictionary<string, Action>
            {
                ["View product"] = () => _adminService.ViewProduct(Data.Products),
                ["Search product"] = () =>
                {
                    Console.WriteLine(_adminService.SearchProduct(_consoleHandler.GetNameOfProduct(), Data.Products));
                },
                ["Change order status"] = () =>
                {
                    User user = _consoleHandler.ChoseUserForOrderStatusChanging(Data.Users);
                    Order order = _consoleHandler.ChooseOrder(user.Orders);

                    if (order == null)
                        throw new ArgumentNullException();

                    _adminService.ChangeOrderStatusAdmin(user, order, _consoleHandler.ChoseOrderStatusAdmin());
                },
                ["See order"] = () =>
                {
                    foreach (var item in _adminService.SeeOwnOrder(Data.GetUser()))
                    {
                        Console.WriteLine(item);
                    }
                },
                ["Create new order"] = () =>
                {
                    _adminService.ViewProduct(Data.Products);
                    _adminService.CreateNewOrder(_consoleHandler.CreateNewOrder(Data.Products), Data.GetUser());
                },
                ["See user info"] = () =>
                {
                    _adminService.SeeUserInformation(Data.Users);
                },
                ["Change user info"] = () =>
                {
                    _adminService.SeeUserInformation(Data.Users);
                    (User, User) newAndCurentUser = _consoleHandler.GetUserData(Data.Users);
                    _adminService.ChangeUserInformation(newAndCurentUser.Item2, newAndCurentUser.Item1, Data.Users);
                },
                ["Change product info"] = () =>
                {
                    _adminService.ViewProduct(Data.Products);
                    (Product, Product) newAndCurentProd = _consoleHandler.GetProductDataForChanging(Data.Products);
                    _adminService.ChangeProductInfo(newAndCurentProd.Item2, newAndCurentProd.Item1, Data.Products);
                },
                ["Add new product"] = () =>
                {
                    _adminService.AddNewProduct(_consoleHandler.GetProducData(), Data.Products);
                },
                ["Exit"] = () =>
                {
                    _adminService.Exit(Data);
                    _interfaceShow.Start();
                }
            };
        }
    }
}
