﻿using System;
using System.Collections.Generic;
using ConsoleEShop.Interfaces;
using ConsoleEShop.Repositories;
using ConsoleEShop.Services;

namespace ConsoleEShop.Menus
{
    public class UserMenu : IShow
    {
        public bool IsActive { get; set; }
        public string[] UserAction = { "View product",
            "Search product",
            "Change order status",
            "See order",
            "Create new order",
            "Accept or canceled order",
            "Exit" };
        public Repository Data { get; set; }

        private readonly IShow _interfaceShow;
        private readonly IUserService _userService;
        private readonly ConsoleHandler _consoleHandler;
        private Dictionary<string, Action> _userShowAction;

        public UserMenu(Repository data, IUserService userService)
        {
            Data = data;
            _userService = userService;
            _consoleHandler = new ConsoleHandler();
            _interfaceShow = new GuestMenu(Data, new GuestService());
        }

        public void Start()
        {
            IsActive = true;

            Console.WriteLine("Hello");
            FillInDictionary();

            while (IsActive)
            {
                Console.Clear();
                _consoleHandler.ShowFunction(UserAction);
                Console.WriteLine("Choose action");

                string action = Console.ReadLine().Trim();

                if (_userShowAction.ContainsKey(action))
                {
                    _userShowAction[action].Invoke();
                    Console.WriteLine("Enter for continue");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Try again");
                }
            }
        }

        private void FillInDictionary()
        {
            _userShowAction = new Dictionary<string, Action>
            {
                ["View product"] = () => _userService.ViewProduct(Data.Products),
                ["Search product"] = () =>
                {
                    Console.WriteLine(_userService.SearchProduct(_consoleHandler.GetNameOfProduct(), Data.Products));
                },
                ["Change order status"] = () => _userService.ChangeOrderStatusUser(Data.GetUser(), _consoleHandler.ChooseOrder(Data.GetUser().Orders)),
                ["See order"] = () =>
                {
                    foreach (var item in _userService.SeeOwnOrder(Data.GetUser()))
                    {
                        Console.WriteLine(item);
                    }
                },
                ["Create new order"] = () =>
                {
                    _userService.ViewProduct(Data.Products);
                    _userService.CreateNewOrder(_consoleHandler.CreateNewOrder(Data.Products), Data.GetUser());
                },
                ["Accept or canceled order"] = () =>
                {
                    _userService.ViewProduct(Data.Products);
                    _userService.CreateNewOrder(_consoleHandler.CreateNewOrder(Data.Products), Data.GetUser());
                },
                ["Exit"] = () =>
                {
                    _userService.Exit(Data);
                    _interfaceShow.Start();
                }
            };
        }
    }
}
