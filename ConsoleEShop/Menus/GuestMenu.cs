﻿using System;
using System.Collections.Generic;
using ConsoleEShop.Repositories;
using ConsoleEShop.Interfaces;

namespace ConsoleEShop.Menus
{
    public class GuestMenu : IShow
    {
        public bool IsActive { get; set; }
        public string[] UserAction = { "Login", "Registration", "View product", "Search product", "Exit" };
        public Repository Data { get; set; }

        private IShow _interfaceShow;
        private readonly IGuestService _guestService;
        private ConsoleHandler _consoleHandler;
        private Dictionary<string, Action> _guestShowAction;

        public GuestMenu(Repository data, IGuestService guestService)
        {
            Data = data;
            _guestService = guestService;
            _consoleHandler = new ConsoleHandler();
        }

        public void Start()
        {
            IsActive = true;
            Console.WriteLine("Hello");

            FillInDictionary();

            while (IsActive)
            {
                Console.Clear();
                _consoleHandler.ShowFunction(UserAction);
                Console.WriteLine("Choose action");
                string action = Console.ReadLine().Trim();

                if (_guestShowAction.ContainsKey(action))
                {
                    _guestShowAction[action].Invoke();
                    Console.WriteLine("Enter for continue");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Try again");
                }
            }
        }
        private void FillInDictionary()
        {
            _guestShowAction = new Dictionary<string, Action>
            {
                ["Login"] = () => _guestService.Login(Data, _interfaceShow, _guestService.IsLogin(_consoleHandler.GetUserLogin(), Data)),
                ["Registration"] = () => _guestService.Registration(_consoleHandler.UserRegistration(), Data),
                ["View product"] = () => _guestService.ViewProduct(Data.Products),
                ["Search product"] = () =>
                {
                    Console.WriteLine(_guestService.SearchProduct(_consoleHandler.GetNameOfProduct(), Data.Products));
                    Console.WriteLine();
                },
                ["Exit"] = () =>
                {
                    Console.WriteLine("Good bay");
                    IsActive = false;
                    Environment.Exit(0);
                }

            };
        }
    }
}
