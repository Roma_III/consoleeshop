﻿using System;
using System.Collections.Generic;
using ConsoleEShop.Repositories;
using ConsoleEShop.Interfaces;
using ConsoleEShop.Menus;

namespace ConsoleEShop.Services
{
    public class GuestService : IGuestService
    {
        public bool IsLogin((string, string) source, Repository data)
        {
            if (data.Users.Find(u => u.Name == source.Item1 && u.Pass == source.Item2) != null)
            {
                data.SetUser(data.Users.Find(u => u.Name == source.Item1 && u.Pass == source.Item2));
                return true;
            }
            return false;

        }
        public void Registration(User user, Repository data)
        {
            if (user != null)
            {
                data.Users.Add(user);
            }
        }
        public void ViewProduct(List<Product> products)
        {
            int count = 1;
            if (products != null)
            {
                foreach (var prod in products)
                {
                    Console.WriteLine($"{count} {prod}");
                    count++;
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("We havent any products");
            }

        }
        public Product SearchProduct(string name, List<Product> products)
        {
            return products.Find(p => p.Name == name);
        }
        public void Login(Repository data, IShow interfaceShow, bool isLogin)
        {
            if (data.GetUser()?.Role == Roles.User && isLogin)
            {
                interfaceShow = new UserMenu(data, new UserService());
                Console.Clear();
                interfaceShow.Start();
            }
            else if (isLogin)
            {
                interfaceShow = new AdminMenu(data, new AdminService());
                Console.Clear();
                interfaceShow.Start();
            }
            else
            {
                interfaceShow = new GuestMenu(data, new GuestService());
                Console.Clear();
                Console.WriteLine("Incorec user Data");
                interfaceShow.Start();

            }
        }
    }
}
