﻿using System;
using System.Collections.Generic;
using ConsoleEShop.Repositories;
using ConsoleEShop.Interfaces;

namespace ConsoleEShop.Services
{
    public class UserService : GuestService, IUserService
    {
        public Order GetOrder(int number, User user)
        {
            return user.Orders[number - 1];
        }
        public void ChangeOrderStatusUser(User user, Order order)
        {
            if (user.Role == Roles.User)
            {
                user.Orders.Find(o => o == order).Status = OrderStatus.CanseledByUser;
            }
        }
        public void CreateNewOrder(Order order, User user)
        {
            if (order != null)
            {
                user.Orders.Add(order);
            }
        }
        public void Exit(Repository data)
        {
            data.SetUser(null);
        }
        public List<Order> SeeOwnOrder(User user)
        {
            if (user != null && user.Orders != null && user.Orders.Count > 0)
                return user.Orders;

            throw new ArgumentNullException();
        }
        public void FinishOrder(Order order, OrderIsAccept orderIsAccept)
        {
            if (order != null)
            {
                order.OrderIsAccepteble = orderIsAccept;
            }
        }
    }
}
