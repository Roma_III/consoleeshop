﻿using System;
using System.Collections.Generic;
using ConsoleEShop.Repositories;
using ConsoleEShop.Interfaces;

namespace ConsoleEShop.Services
{
    public class AdminService : UserService, IAdminService
    {
        public void AddNewProduct(Product product, List<Product> products)
        {
            if (product != null && products != null)
            {
                products.Add(product);
            }
        }
        public void ChangeProductInfo(Product currentProd, Product productAfterChange, List<Product> products)
        {
            if (currentProd != null && productAfterChange != null && products != null)
            {
                products.Find(p => p == currentProd).Name = productAfterChange.Name;
                products.Find(p => p == currentProd).Category = productAfterChange.Category;
                products.Find(p => p == currentProd).Description = productAfterChange.Description;
                products.Find(p => p == currentProd).Price = productAfterChange.Price;
            }
        }
        public void ChangeUserInformation(User currentUser, User userAfterChange, List<User> users)
        {
            if (currentUser != null && userAfterChange != null && users != null)
            {
                users.Find(p => p == currentUser).Name = userAfterChange.Name;
                users.Find(p => p == currentUser).Pass = userAfterChange.Pass;
                users.Find(p => p == currentUser).Role = userAfterChange.Role;
            }
        }
        public void ChangeOrderStatusAdmin(User user, Order order, OrderStatus orderStatus)
        {
            if (order != null && orderStatus != OrderStatus.CanseledByUser)
            {
                user.Orders.Find(o => o == order).Status = orderStatus;
            }
        }
        public void SeeUserInformation(List<User> users)
        {
            if (users != null && users.Count != 0)
            {
                int count = 1;
                foreach (var user in users)
                {
                    Console.WriteLine($"{count} {user.Name} {user.Role}");
                    count++;
                }
            }
        }
        public void ChangeUserRole(User user, Roles role)
        {
            if (user != null)
                user.Role = role;
        }
    }
}
