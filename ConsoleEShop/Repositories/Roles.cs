﻿namespace ConsoleEShop.Repositories
{
    public enum Roles
    {
        Guest,
        User,
        Admin
    }
}
