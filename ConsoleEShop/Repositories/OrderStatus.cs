﻿namespace ConsoleEShop.Repositories
{
    public enum OrderStatus
    {
        CanseledByAdministrator,
        New,
        GetPayment,
        Sent,
        Received,
        Finish,
        CanseledByUser
    }
}
