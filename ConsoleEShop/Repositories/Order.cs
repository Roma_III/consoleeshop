﻿namespace ConsoleEShop.Repositories
{
    public class Order
    {
        public Product Product { get; set; }
        public int Count { get; set; }
        public OrderStatus Status { get; set; }
        public OrderIsAccept OrderIsAccepteble { get; set; }
        public Order(Product product, int count)
        {
            Product = product;
            Count = count;
            Status = OrderStatus.New;
            OrderIsAccepteble = OrderIsAccept.New;
        }
        public override string ToString()
        {
            return $"{Product.ToString()} {Count} {Status}";
        }
    }
    public enum OrderIsAccept
    {
        Accept,
        Canceled,
        New
    }
}
