﻿using System.Collections.Generic;

namespace ConsoleEShop.Repositories
{
    public class Repository
    {
        public List<Product> Products { get; set; }
        public List<User> Users { get; set; }

        private User CurrentUser { get; set; }
        private static readonly object _lock = new object();
        private static Repository _instance;

        private Repository() { }
        public Repository(List<Product> products, List<User> users, User user)
        {
            Products = products;
            Users = users;
            CurrentUser = user;
        }
        public void SetUser(User user)
        {
            CurrentUser = user;
        }
        public User GetUser()
        {
            return CurrentUser;
        }
        public static Repository GetInstance(List<Product> products, List<User> users, User user)
        {
            if (_instance == null)
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new Repository();
                        _instance.Products = products;
                        _instance.Users = users;
                        _instance.CurrentUser = user;
                    }
                }
            }
            return _instance;
        }
    }
}
