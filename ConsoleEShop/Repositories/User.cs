﻿using System.Collections.Generic;

namespace ConsoleEShop.Repositories
{
    public class User
    {
        public string Name { get; set; }
        public string Pass { get; set; }
        public List<Order> Orders { get; set; }
        public Roles Role { get; set; }
        public User(string name, string pass, List<Order> orders, Roles role)
        {
            Name = name;
            Pass = pass;
            Orders = orders;
            Role = role;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
