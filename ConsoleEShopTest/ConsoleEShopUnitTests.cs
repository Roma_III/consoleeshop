using ConsoleEShop.Repositories;
using ConsoleEShop.Services;
using System.Collections.Generic;
using System;
using Xunit;

namespace ConsoleEShopTest
{
    public class ConsoleEShopUnitTests
    {
        public class GuestServise : GuestService
        {
            Repository data;
            [Fact]
            public void RegistrationTest()
            {
                //Arrage
                data = GetTestData();
                User user = new User("Roma", "Roma", new List<Order>(), Roles.User);
                //Act
                Registration(user, data);
                User expectedUser = data.Users.Find(u => u.Name == user.Name && u.Pass == user.Pass);
                //Assert
                Assert.Equal(expectedUser, user);
            }
            [Fact]
            public void LoginTest()
            {
                //Arrage
                data = GetTestData();
                User user = data.Users[0];
                //Act
                IsLogin((user.Name, user.Pass), data);
                User result = user;
                User expecterResult = data.GetUser();
                //Assert
                Assert.Equal(expecterResult, result);
            }
            [Fact]
            public void SearchProductTest()
            {
                //Arrage
                data = GetTestData();
                User user = data.Users[0];
                //Act
                Product result = SearchProduct("Apple", data.Products);
                Product expecterResult = data.Products[0];
                //Assert
                Assert.Equal(expecterResult, result);
            }
            public Repository GetTestData()
            {
                List<Product> products = new List<Product>() {
                new Product("Apple", "Fruit", "Green", 1000),
                new Product("Milk", "Grocery", "White", 2000),
            };
                List<User> users = new List<User>() {
                new User("Roman", "123", new List<Order>{ new Order(products[1], 1)}, Roles.User),
                new User("Vova", "111", new List<Order>(), Roles.User),
                new User("Boris", "admin", new List<Order>(), Roles.Admin)
            };
                Repository data = Repository.GetInstance(products, users, null);
                return data;
            }
        }
        public class UserServiseTest : UserService
        {//Arrage
         //Act
         //Assert
            Repository data;
            [Fact]
            public void ChangeOrderStatusUserTest()
            {

                //Arrage
                data = GetTestData();
                data.SetUser(data.Users[0]);
                Order order = data.GetUser().Orders[0];
                //Act
                ChangeOrderStatusUser(data.GetUser(), order);
                OrderStatus result = data.GetUser().Orders[0].Status;
                OrderStatus expected = OrderStatus.CanseledByUser;
                //Assert
                Assert.Equal(expected, result);
            }
            [Fact]
            public void CreateNewOrderTest()
            {
                //Arrage
                data = GetTestData();
                data.SetUser(data.Users[0]);
                Order expected = new Order(new Product("Tomato", "Vegetables", "Red", 123), 2);
                //Act
                CreateNewOrder(expected, data.GetUser());
                //Assert
                Order result = data.GetUser().Orders[1];
                Assert.Equal(expected, result);
            }
            [Fact]
            public void ExitTest()
            {
                //Arrage
                data = GetTestData();
                data.SetUser(data.Users[0]);
                //Act
                Exit(data);
                //Assert
                Assert.Null(data.GetUser());
            }
            [Fact]
            public void SeeOwnOrderTest()
            {
                //Arrage
                data = GetTestData();
                data.SetUser(data.Users[0]);
                User user = data.GetUser();
                //Act

                //Assert
                Assert.NotNull(SeeOwnOrder(user));
            }

            public Repository GetTestData()
            {
                List<Product> products = new List<Product>() {
                new Product("Apple", "Fruit", "Green", 1000),
                new Product("Milk", "Grocery", "White", 2000),
            };
                List<User> users = new List<User>() {
                new User("Roman", "123", new List<Order>{ new Order(products[1], 1)}, Roles.User),
                new User("Vova", "111", new List<Order>(), Roles.User),
                new User("Boris", "admin", new List<Order>(), Roles.Admin)
            };
                Repository data = Repository.GetInstance(products, users, null);
                return data;
            }
        }

        public class AdminServiceTest : AdminService
        {//Arrage
         //Act
         //Assert
            Repository data;
            [Fact]
            public void AddNewProductTest()
            {
                //Arrage
                data = GetTestData();
                Product expected = new Product("Tomato", "Vegetables", "Red", 500);
                List<Product> products = data.Products;
                //Act
                AddNewProduct(expected, products);
                Product result = data.Products[products.Count - 1];
                //Assert
                Assert.Equal(expected, result);
            }

            [Fact]
            public void ChangeProductInfoTest()
            {
                //Arrage
                int index = 0;
                data = GetTestData();
                List<Product> products = data.Products;
                Product expected = new Product("Tomato", "Vegetables", "Red", 500);
                Product result = products[index];

                //Act
                ChangeProductInfo(products[index], expected, products);

                //Assert
                Assert.Equal(expected.Name, result.Name);
                Assert.Equal(expected.Description, result.Description);
                Assert.Equal(expected.Category, result.Category);
                Assert.Equal(expected.Price, result.Price);

            }

            [Fact]
            public void ChangeUsertInfoTest()
            {
                //Arrage
                int index = 0;
                data = GetTestData();
                List<User> users = data.Users;
                User expected = new User("ROMAN", "2402", new List<Order>(), Roles.User);
                User result = users[index];

                //Act
                ChangeUserInformation(users[index], expected, users);

                //Assert
                Assert.Equal(expected.Name, result.Name);
                Assert.Equal(expected.Pass, result.Pass);
                Assert.Equal(expected.Role, result.Role);

            }

            [Fact]
            public void ChangeOrderStatusAdminTest()
            {
                //Arrage
                data = GetTestData();
                User user = data.Users[0];
                Order order = user.Orders[0];
                OrderStatus expected = OrderStatus.GetPayment;

                //Act
                ChangeOrderStatusAdmin(user, order, expected);
                //Assert
                Assert.Equal(expected, order.Status);

            }

            [Fact]
            public void ChangeUserRoleTest()
            {
                //Arrage
                data = GetTestData();
                User user = data.Users[0];
                Roles role = Roles.Admin;
                //Act
                ChangeUserRole(user, role);
                //Assert

                Assert.Equal(role, user.Role);
            }
            public Repository GetTestData()
            {
                List<Product> products = new List<Product>() {
                new Product("Apple", "Fruit", "Green", 1000),
                new Product("Milk", "Grocery", "White", 2000),
            };
                List<User> users = new List<User>() {
                new User("Roman", "123", new List<Order>{ new Order(products[1], 1)}, Roles.User),
                new User("Vova", "111", new List<Order>(), Roles.User),
                new User("Boris", "admin", new List<Order>(), Roles.Admin)
            };
                Repository data = Repository.GetInstance(products, users, null);
                return data;
            }
        }
    }
}
